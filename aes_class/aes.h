//
// Created by TB18008 on 20.11.2021..
//

#ifndef AES_128_AES_H
#define AES_128_AES_H

#include <iostream>
#include "key_block.h"
#include "block.h"
#include <sstream>

class AES {
    public:
    AES();  //Constructor

    //Methods
    void encryptBlock(const string& plaintext_string, const string& key_string, string& ciphertext_result_string, bool verbose = false);
    void encryptBlock(const char (&plaintext)[33], const char (&key)[33], char (&ciphertext_result)[33], bool verbose = false);
    Block encryptBlock(Block plaintext_block, KeyBlock key_block, bool verbose = false);

    void decryptBlock(const string& ciphertext_string, const string& key_string, string& plaintext_result_string, bool verbose = false);
    void decryptBlock(const char (&ciphertext)[33], const char (&key)[33], char (&plaintext_result)[33], bool verbose = false);
    Block decryptBlock(Block ciphertext_block, KeyBlock key_block, bool verbose = false);

    KeyBlock getRoundKeyFromStart(KeyBlock original_key, int round_number);

    static void writeOutBlock(Block block, char (&char_array)[33]);
};


#endif //AES_128_AES_H
