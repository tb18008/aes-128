//
// Created by TB18008 on 14.11.2021..
//

#include "byte.h"

//CONSTRUCTORS
Byte::Byte(int value) {
    setValue(value);
}
Byte::Byte(const string& hex) {
    string byte_string; //String containing a pair of hex characters
    byte_string = "";
    byte_string += hex[0];
    byte_string += hex[1];
    this->setValue(stoi(byte_string, nullptr, 16));
}

//METHODS
void Byte::setValue(int new_value) {

    this->value = new_value;

    if(this->value == 0) {
        for (int i = 0; i < 8; ++i) this->bits[i]=false;
        return;
    }

    for (bool & bit : this->bits) bit = false;  //Need to zero out all bits
    //Set bit boolean values
    int number = this->value;
    for(int i=0; number>0; i++)
    {
        this->bits[i]=number % 2;
        number = number / 2;
    }
}
Byte Byte::substituteByte(bool inverse) {
    if(!inverse) return Byte(aes_byte_sbox[this->value]);
    return Byte(inv_aes_byte_sbox[this->value]);
}
Byte Byte::shiftDiscard(bool left) {
    Byte new_byte;
    if(left) new_byte.setValue((this->value * 2) % 256);
    else new_byte.setValue(this->value / 2);
    return new_byte;
}

//OPERATORS

bool operator==(const Byte &byte_a, const Byte &byte_b) {
    if(byte_a.value!=byte_b.value) return false;
    for(int i=0; i<8; i++) if(byte_a.bits[i]!=byte_b.bits[i]) return false;
    return true;
}

bool operator!=(const Byte &byte_a, const Byte &byte_b) {
    return !(byte_a==byte_b);
}

ostream &operator<<(ostream &os, Byte byte) {
    if(byte.value < 16) os << "0" << hex << byte.value;
    else os << hex << byte.value;
    return os;
}
Byte operator xor(Byte byte_a, Byte byte_b) {
    Byte new_byte = Byte(byte_a.value ^ byte_b.value);
    return new_byte;
}

Byte operator xor(Byte *byte_a, Byte byte_b) {
    return *byte_a xor byte_b;
}
Byte operator xor(Byte byte_a, Byte *byte_b) {
    return byte_a xor *byte_b;
}

Byte operator*(Byte byte_a, Byte byte_b) {
    //https://en.wikipedia.org/wiki/Finite_field_arithmetic#Multiplication

    bool carry;
    Byte irreducible = Byte(0x1b);

    Byte product = Byte(0);
    if(byte_a.value && byte_b.value){ //Run calculation only if both operands are not 0
        for (int i = 0; i < 8; ++i) {
            if(byte_b.value % 2) product = product xor byte_a; //Check if rightmost bit is true
            byte_b = byte_b.shiftDiscard(false);
            carry = byte_a.value > 127;    //Leftmost bit is true
            byte_a = byte_a.shiftDiscard(true);
            if(carry) byte_a = byte_a xor irreducible;
        }
    }
    return product;
}
Byte operator*(int byte_value, Byte byte_b) {
    if(byte_value == 1) return Byte(byte_value);
    return Byte(byte_value) * byte_b;
}
Byte operator*(Byte byte_a, int byte_value) {
    if(byte_value == 1) return Byte(byte_value);
    return byte_a * Byte(byte_value);
}
