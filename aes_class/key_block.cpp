//
// Created by TB18008 on 20.11.2021..
//

#include "key_block.h"

KeyBlock::KeyBlock() = default;
KeyBlock::KeyBlock(const string &hex) : Block(hex) {}
KeyBlock::KeyBlock(const char (&hex_char_array)[32]) : Block(hex_char_array) {}

KeyBlock KeyBlock::getRoundKey(int round_number){
    KeyBlock new_key;
    KeyBlock old_key = *this;
    new_key.setBlock(*this);

    if(round_number == 0) return new_key;

    //Set up round constant
    Byte round_constant_bytes[4];
    for (int i = 1; i < 4; ++i) round_constant_bytes[i] = Byte(0);
    round_constant_bytes[0] = Byte(round_constants[round_number-1]);
    Word round_constant = Word(round_constant_bytes);

    //Change words of new key
    Word temp = new_key.columns[3];
    temp.setWord(this->functionG(temp, round_constant));
    new_key.columns[0].setWord(old_key.columns[0] xor temp);
    new_key.columns[1].setWord(old_key.columns[1] xor new_key.columns[0]);
    new_key.columns[2].setWord(old_key.columns[2] xor new_key.columns[1]);
    new_key.columns[3].setWord(old_key.columns[3] xor new_key.columns[2]);

    return new_key;
}

Word KeyBlock::functionG(Word temp, Word round_constant) {
    Word new_word = temp;

    new_word = new_word.circularShift(true);
    new_word = new_word.subBytes(false);
    new_word = new_word xor round_constant;

    return new_word;
}