//
// Created by TB18008 on 20.11.2021..
//

#include "aes.h"

AES::AES()= default;

void AES::encryptBlock(const char (&plaintext)[33], const char (&key)[33], char (&ciphertext_result)[33], bool verbose) {
    Block cipher_text_block = Block(plaintext);
    KeyBlock key_block = KeyBlock(key);

    if(verbose){
        cout << "round[ 0].input\t\t"<<cipher_text_block<<endl;
        cout << "round[ 0].k_sch\t\t"<<key_block<<endl;
    }

    //Round 0
    cipher_text_block = cipher_text_block xor key_block;
    if(verbose) cout << "round[ 1].start\t\t"<<cipher_text_block<<endl;
    //Round 1 - 9
    for (int i = 1; i < 10; ++i) {
        cipher_text_block = cipher_text_block.substituteBytes();
        if(verbose) cout << "round[ "<<i<<"].s_box\t\t"<<cipher_text_block<<endl;
        cipher_text_block = cipher_text_block.shiftRows();
        if(verbose) cout << "round[ "<<i<<"].s_row\t\t"<<cipher_text_block<<endl;
        cipher_text_block = cipher_text_block.mixColumns();
        if(verbose) cout << "round[ "<<i<<"].m_col\t\t"<<cipher_text_block<<endl;
        //Get round key
        key_block.setBlock(key_block.getRoundKey(i));
        if(verbose) cout << "round[ "<<i<<"].k_sch\t\t"<<key_block<<endl;
        cipher_text_block = cipher_text_block xor key_block;
        if(verbose) cout << "round[ "<<i+1<<"].start\t\t"<<key_block<<endl;
    }
    //Round 10
    cipher_text_block = cipher_text_block.substituteBytes();
    if(verbose) cout << "round[10].s_box\t\t"<<cipher_text_block<<endl;
    cipher_text_block = cipher_text_block.shiftRows();
    if(verbose) cout << "round[10].s_row\t\t"<<cipher_text_block<<endl;
    //Get round key
    key_block.setBlock(key_block.getRoundKey(10));
    if(verbose) cout << "round[10].k_sch\t\t"<<key_block<<endl;
    cipher_text_block = cipher_text_block xor key_block;
    if(verbose) cout << "round[10].output\t\t"<<cipher_text_block<<endl;

    this->writeOutBlock(cipher_text_block,ciphertext_result);
}
void AES::decryptBlock(const char (&ciphertext)[33], const char (&key)[33], char (&plaintext_result)[33], bool verbose) {
    Block plaintext_block = Block(ciphertext);
    KeyBlock original_key = KeyBlock(key);  //Keep the 0th key in memory to calculate round keys in reverse
    KeyBlock key_block;

    key_block.setBlock(getRoundKeyFromStart(original_key,11));

    if(verbose){
        cout << "round[ 0].iinput\t\t"<<plaintext_block<<endl;
        cout << "round[ 0].ik_sch\t\t"<<key_block<<endl;
    }

    //Round 0
    plaintext_block = plaintext_block xor key_block;
    if(verbose) cout << "round[ 1].istart\t\t"<<plaintext_block<<endl;
    //Round 1 - 9
    for (int i = 1; i < 10; ++i) {
        plaintext_block = plaintext_block.shiftRows(true);
        if(verbose) cout << "round[ "<<i<<"].is_row\t\t"<<plaintext_block<<endl;
        plaintext_block = plaintext_block.substituteBytes(true);
        if(verbose) cout << "round[ "<<i<<"].is_box\t\t"<<plaintext_block<<endl;
        //Get round key
        key_block.setBlock(getRoundKeyFromStart(original_key,11-i));
        if(verbose) cout << "round[ "<<i<<"].ik_sch\t\t"<<key_block<<endl;
        plaintext_block = plaintext_block xor key_block;
        if(verbose) cout << "round[ "<<i+1<<"].istart\t\t"<<key_block<<endl;


        plaintext_block = plaintext_block.mixColumns(true);
        if(verbose) cout << "round[ "<<i<<"].im_col\t\t"<<plaintext_block<<endl;
    }
    //Round 10
    plaintext_block = plaintext_block.shiftRows(true);
    if(verbose) cout << "round[10].is_row\t\t"<<plaintext_block<<endl;
    plaintext_block = plaintext_block.substituteBytes(true);
    if(verbose) cout << "round[10].is_box\t\t"<<plaintext_block<<endl;
    //Get round key
    if(verbose) cout << "round[10].ik_sch\t\t"<<original_key<<endl;
    plaintext_block = plaintext_block xor original_key;
    if(verbose) cout << "round[10].ioutput\t\t"<<plaintext_block<<endl;

    this->writeOutBlock(plaintext_block,plaintext_result);
}
KeyBlock AES::getRoundKeyFromStart(const KeyBlock original_key, int round_number){
    KeyBlock new_key;
    new_key.setBlock(original_key);

    for (int i = 0; i < round_number; ++i) new_key.setBlock(new_key.getRoundKey(i));

    return new_key;
};

void AES::encryptBlock(const string &plaintext_string, const string &key_string, string &ciphertext_result_string, bool verbose) {
    //Convert strings to character arrays
    char plaintext[33], key[33], ciphertext[33];
    for (int i = 0; i < 32; ++i) {
        plaintext[i] = plaintext_string[i];
        key[i] = key_string[i];
    }
    plaintext[32] = '\0';
    key[32] = '\0';

    //Get block from regular function
    this->encryptBlock(plaintext, key, ciphertext, verbose);

    //Print out result on the string
    for (int i = 0; i < 32; ++i) ciphertext_result_string[i] = ciphertext[i];
}

Block AES::encryptBlock(Block plaintext_block, KeyBlock key_block, bool verbose){
    Block cipher_text_block;
    cipher_text_block.setBlock(plaintext_block);

    if(verbose){
        cout << "round[ 0].input\t\t"<<cipher_text_block<<endl;
        cout << "round[ 0].k_sch\t\t"<<key_block<<endl;
    }

    //Round 0
    cipher_text_block = cipher_text_block xor key_block;
    if(verbose) cout << "round[ 1].start\t\t"<<cipher_text_block<<endl;
    //Round 1 - 9
    for (int i = 1; i < 10; ++i) {
        cipher_text_block = cipher_text_block.substituteBytes();
        if(verbose) cout << "round[ "<<i<<"].s_box\t\t"<<cipher_text_block<<endl;
        cipher_text_block = cipher_text_block.shiftRows();
        if(verbose) cout << "round[ "<<i<<"].s_row\t\t"<<cipher_text_block<<endl;
        cipher_text_block = cipher_text_block.mixColumns();
        if(verbose) cout << "round[ "<<i<<"].m_col\t\t"<<cipher_text_block<<endl;
        //Get round key
        key_block.setBlock(key_block.getRoundKey(i));
        if(verbose) cout << "round[ "<<i<<"].k_sch\t\t"<<key_block<<endl;
        cipher_text_block = cipher_text_block xor key_block;
        if(verbose) cout << "round[ "<<i+1<<"].start\t\t"<<key_block<<endl;
    }
    //Round 10
    cipher_text_block = cipher_text_block.substituteBytes();
    if(verbose) cout << "round[10].s_box\t\t"<<cipher_text_block<<endl;
    cipher_text_block = cipher_text_block.shiftRows();
    if(verbose) cout << "round[10].s_row\t\t"<<cipher_text_block<<endl;
    //Get round key
    key_block.setBlock(key_block.getRoundKey(10));
    if(verbose) cout << "round[10].k_sch\t\t"<<key_block<<endl;
    cipher_text_block = cipher_text_block xor key_block;
    if(verbose) cout << "round[10].output\t\t"<<cipher_text_block<<endl;

    return cipher_text_block;
}
void AES::decryptBlock(const string &ciphertext_string, const string &key_string, string &plaintext_result_string, bool verbose) {
    //Convert strings to character arrays
    char ciphertext[33], plaintext[33], key[33];
    for (int i = 0; i < 32; ++i) {
        ciphertext[i] = ciphertext_string[i];
        key[i] = key_string[i];
    }
    ciphertext[32] = '\0';
    key[32] = '\0';

    //Get block from regular function
    this->decryptBlock(ciphertext, key, plaintext, verbose);

    //Print out result on the string
    for (int i = 0; i < 32; ++i) plaintext_result_string[i] = plaintext[i];
}

Block AES::decryptBlock(Block ciphertext_block, KeyBlock key_block, bool verbose) {
    Block plaintext_block;
    plaintext_block.setBlock(ciphertext_block);
    KeyBlock original_key;
    original_key.setBlock(key_block);   //Keep the 0th key in memory to calculate round keys in reverse

    key_block.setBlock(getRoundKeyFromStart(original_key,11));

    if(verbose){
        cout << "round[ 0].iinput\t\t"<<plaintext_block<<endl;
        cout << "round[ 0].ik_sch\t\t"<<key_block<<endl;
    }

    //Round 0
    plaintext_block = plaintext_block xor key_block;
    if(verbose) cout << "round[ 1].istart\t\t"<<plaintext_block<<endl;
    //Round 1 - 9
    for (int i = 1; i < 10; ++i) {
        plaintext_block = plaintext_block.shiftRows(true);
        if(verbose) cout << "round[ "<<i<<"].is_row\t\t"<<plaintext_block<<endl;
        plaintext_block = plaintext_block.substituteBytes(true);
        if(verbose) cout << "round[ "<<i<<"].is_box\t\t"<<plaintext_block<<endl;
        //Get round key
        key_block.setBlock(getRoundKeyFromStart(original_key,11-i));
        if(verbose) cout << "round[ "<<i<<"].ik_sch\t\t"<<key_block<<endl;
        plaintext_block = plaintext_block xor key_block;
        if(verbose) cout << "round[ "<<i+1<<"].istart\t\t"<<key_block<<endl;


        plaintext_block = plaintext_block.mixColumns(true);
        if(verbose) cout << "round[ "<<i<<"].im_col\t\t"<<plaintext_block<<endl;
    }
    //Round 10
    plaintext_block = plaintext_block.shiftRows(true);
    if(verbose) cout << "round[10].is_row\t\t"<<plaintext_block<<endl;
    plaintext_block = plaintext_block.substituteBytes(true);
    if(verbose) cout << "round[10].is_box\t\t"<<plaintext_block<<endl;
    //Get round key
    if(verbose) cout << "round[10].ik_sch\t\t"<<original_key<<endl;
    plaintext_block = plaintext_block xor original_key;
    if(verbose) cout << "round[10].ioutput\t\t"<<plaintext_block<<endl;

    return plaintext_block;
}
void AES::writeOutBlock(Block block, char (&char_array)[33]) {

    //From stringstream to string
    string result_string;
    result_string.resize(32);
    stringstream ss;
    ss << block;
    result_string = ss.str();

    //From string to character array
    for (int i = 0; i < 32; ++i) char_array[i] = result_string[i];
    char_array[32] = '\0';
    return;
}
