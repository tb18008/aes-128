//
// Created by TB18008 on 15.11.2021..
//

#ifndef AES_128_WORD_H
#define AES_128_WORD_H

#include "byte.h"

class Word {
    public:
        //PROPERTIES
        Byte* bytes[4]{};

        //CONSTRUCTORS
        Word();
        explicit Word(Byte* byte_array);
        explicit Word(Byte* byte_array, int offset);

        //METHODS
        int getByte(int byte_index);
        Byte byte(int byte_index);
        void setWord(Word new_word);
        Word circularShift(bool left = false);   //Moves bytes to the left (right if bool set to false)
        Word subBytes(bool inverse = false);
        Word mixWord(bool inverse = false);

        //OPERATORS
        friend ostream &operator<<(ostream &os, Word word);
        //Apply exclusive or to each byte and return the resulting word
        friend Word operator xor(Word word_a, Word word_b);
};


#endif //AES_128_WORD_H
