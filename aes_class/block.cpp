//
// Created by TB18008 on 14.11.2021..
//

#include "block.h"

//CONSTRUCTORS
Block::Block(){
    for (int i = 0; i < 16; ++i) this->bytes[i/4][i%4] = Byte(0);
    this->setRowsAndColumns();
}
Block::Block(const string& hex_string) {
    string char_pair;
    int string_size = 32;
    if(hex_string.length() < 32) string_size = hex_string.length();
    for (int i = 0; i < string_size; i+=2) {
        char_pair = "";
        char_pair += hex_string[i];
        char_pair += hex_string[i + 1];

        //Fill bytes
        this->bytes[(i/2)/4][(i/2)%4] = Byte(char_pair);
    }
    //Fill rest of the bytes as zeros if string was too short
    for (int i = 0; i < 32 - string_size; i++) {
        //Fill bytes
        this->bytes[(i/2)/4][(i/2)%4] = Byte(0);
    }

    this->setRowsAndColumns();
}
Block::Block(const char (&hex_char_array)[32]) {
    string char_pair;
    for (int i = 0; i < 32; i+=2) {
        char_pair = "";
        char_pair += hex_char_array[i];
        char_pair += hex_char_array[i + 1];

        //Fill bytes
        this->bytes[(i/2)/4][(i/2)%4] = Byte(char_pair);
    }

    this->setRowsAndColumns();
}
Block::Block(Byte byte_array[16]) {
    //Fill bytes
    for (int i = 0; i < 16; ++i) this->bytes[i/4][i%4] = byte_array[i];
    this->setRowsAndColumns();
}

//METHODS
int Block::getByte(int byte_index) {
    return this->bytes[byte_index/4][byte_index%4].value;
}
void Block::setBlock(Block new_block) {
    for (int i = 0; i < 16; ++i) this->bytes[i/4][i%4].setValue(new_block.bytes[i/4][i%4].value);
    this->setRowsAndColumns();
}
void Block::setRowsAndColumns() {
    for (int i = 0; i < 4; ++i) this->columns[i] = Word(this->bytes[i]);
    for (int row = 0; row < 4; ++row) this->rows[row] = Word(*this->bytes, row);
}
void Block::printMatrix(){
    int byte_value;

    for (int column = 0; column < 4; ++column) {
        for (int row = 0; row < 4; ++row) {
            byte_value = this->bytes[row][column].value;
            cout << hex << '[';
            if(byte_value < 16) cout << '0';
            cout << byte_value << ']';
        }
        cout << endl;
    }
    cout << dec;
}
Block Block::substituteBytes(bool inverse) {
    Block new_block = Block();
    for (int i = 0; i < 16; ++i) new_block.bytes[i/4][i%4].setValue(this->bytes[i/4][i%4].substituteByte(inverse).value);
    new_block.setRowsAndColumns();
    return new_block;
}
Block Block::shiftRows(bool inverse) {
    Block new_block;
    new_block.setBlock(*this);

    //Don't shift first row

    new_block.rows[1] = new_block.rows[1].circularShift(!inverse);  //If inverse then shift right. Otherwise, shift left

    new_block.rows[2] = new_block.rows[2].circularShift(!inverse);
    new_block.rows[2] = new_block.rows[2].circularShift(!inverse);

    new_block.rows[3] = new_block.rows[3].circularShift(!inverse);
    new_block.rows[3] = new_block.rows[3].circularShift(!inverse);
    new_block.rows[3] = new_block.rows[3].circularShift(!inverse);

    return new_block;
}

Block Block::mixColumns(bool inverse) {
    Block new_block;
    new_block.setBlock(*this);

    //Mix each column
    for (int i = 0; i < 4; ++i) {
        new_block.columns[i].setWord(
                new_block.columns[i].mixWord(inverse)
        );
    }

    return new_block;
}


//OPERATORS
ostream &operator<<(ostream &os, Block block) {
    for (int i = 0; i < 4; ++i) os << block.columns[i];
    return os;
}
bool operator==(const Block &block_a, const Block &block_b) {
    for (int i = 0; i < 16; ++i) if(block_a.bytes[i/4][i%4].value != block_b.bytes[i/4][i%4].value) return false;
    return true;
}
bool operator!=(const Block &block_a, const Block &block_b) {
    return !(block_a == block_b);
}

Block operator xor(Block block_a, Block block_b) {
    Block new_block = Block();
    for (int i = 0; i < 16; ++i) new_block.bytes[i/4][i%4].setValue((block_a.bytes[i/4][i%4] xor block_b.bytes[i/4][i%4]).value);
    new_block.setRowsAndColumns();
    return new_block;
}
