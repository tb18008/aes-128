//
// Created by TB18008 on 14.11.2021..
//

#ifndef AES_128_BLOCK_H
#define AES_128_BLOCK_H

#include "byte.h"
#include "word.h"
#include <iostream>

using namespace std;

class Block {
    public:
        //         x  y
        Byte bytes[4][4];
        Word columns[4];
        Word rows[4];

        //CONSTRUCTORS
        Block();
        Block(const string& hex);
        Block(const char (&hex_char_array)[32]);
        Block(Byte byte_array[16]);

        //METHODS
        int getByte(int byte_index);
        void setRowsAndColumns();
        void setBlock(Block new_block);
        void printMatrix();
        Block substituteBytes(bool inverse = false);
        Block shiftRows(bool inverse = false);
        Block mixColumns(bool inverse = false);

        //OPERATORS
        friend ostream &operator<<(ostream &os, Block block);
        //Compare if the bits of two blocks are the same in every position
        friend bool operator==(const Block& block_a, const Block& block_b);
        friend bool operator!=(const Block& block_a, const Block& block_b);
        //Apply exclusive or to each word and return the resulting block
        friend Block operator xor(Block block_a, Block block_b);

};


#endif //AES_128_BLOCK_H
