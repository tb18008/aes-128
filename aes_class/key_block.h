//
// Created by TB18008 on 20.11.2021..
//

#ifndef AES_128_KEY_BLOCK_H
#define AES_128_KEY_BLOCK_H

#include "block.h"
#include "sbox.h"

static const int round_constants[10] = {
        /* 1     2     3     4     5     6     7     8     9    10*/
        0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36
};

class KeyBlock: public Block {
    public:
    //Constructors
    KeyBlock(const string &hex);
    KeyBlock(const char (&hex_char_array)[32]);
    KeyBlock();

    //Methods
    KeyBlock getRoundKey(int round_number);
    Word functionG(Word temp, Word round_constant);

};


#endif //AES_128_KEY_BLOCK_H
