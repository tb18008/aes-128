//
// Created by TB18008 on 15.11.2021..
//

#include "word.h"

//CONSTRUCTORS
Word::Word() = default;
Word::Word(Byte *byte_array) {
    for (int i = 0; i < 4; ++i) this->bytes[i] = &byte_array[i];
}
Word::Word(Byte *byte_array, int offset) {
    for (int column = 0; column < 4; ++column) this->bytes[column] = &byte_array[column*4+offset];
}

//METHODS
int Word::getByte(int byte_index) {
    return this->bytes[byte_index]->value;
}
Byte Word::byte(int byte) {
    return *this->bytes[byte%4];
}
void Word::setWord(Word new_word) {
    for (int i = 0; i < 4; ++i) this->bytes[i]->setValue(new_word.bytes[i]->value);
}
Word Word::circularShift(bool left) {
    Word new_word = *this;

    int moved_byte_value;

    //Shifting bytes to the left
    if(left){
        moved_byte_value = new_word.bytes[0]->value;    //Save the first byte before it is overwritten
        //Shift last three bytes to left
        for (int i = 1; i < 4; ++i) new_word.bytes[i-1]->setValue(new_word.bytes[i]->value);
        new_word.bytes[3]->setValue(moved_byte_value);   //Move saved byte to the end
    }
        //Shifting bytes to the right
    else{
        moved_byte_value = new_word.bytes[3]->value;    //Save the last byte before it is overwritten
        //Shift last three bytes to left
        for (int i = 3; i >= 0; --i) new_word.bytes[i]->setValue(new_word.bytes[i-1]->value);
        new_word.bytes[0]->setValue(moved_byte_value);   //Move saved byte to beginning
    }

    return new_word;
}
Word Word::subBytes(bool inverse) {
    Word new_word = *this;

    for (int i = 0; i < 4; ++i) new_word.bytes[i]->setValue(new_word.bytes[i]->substituteByte(inverse).value);

    return new_word;
}
Word Word::mixWord(bool inverse) {
    Word column = *this;

    Byte result_word_bytes[4];

    //Gather the new column bytes without replacing the original
    if(!inverse) {
        //FIPS 197 5.1.3 (5.6)
        result_word_bytes[0].setValue((
            0x02 * column.byte(0) xor
            0x03 * column.byte(1) xor
            column.byte(2) xor
            column.byte(3)
        ).value);
        result_word_bytes[1].setValue((
            column.byte(0) xor
            0x02 * column.byte(1) xor
            0x03 * column.byte(2) xor
            column.byte(3)
        ).value);
        result_word_bytes[2].setValue((
            column.byte(0) xor
            column.byte(1) xor
            0x02 * column.byte(2) xor
            0x03 * column.byte(3)
        ).value);
        result_word_bytes[3].setValue((
            0x03 * column.byte(0) xor
            column.byte(1) xor
            column.byte(2) xor
            0x02 * column.byte(3)
        ).value);
    }
    else{
        //FIPS 197 5.3.3 (5.10)
        result_word_bytes[0].setValue((
            0x0e * column.byte(0) xor
            0x0b * column.byte(1) xor
            0x0d * column.byte(2) xor
            0x09 * column.byte(3)
        ).value);
        result_word_bytes[1].setValue((
            0x09 * column.byte(0) xor
            0x0e * column.byte(1) xor
            0x0b * column.byte(2) xor
            0x0d * column.byte(3)
        ).value);
        result_word_bytes[2].setValue((
            0x0d * column.byte(0) xor
            0x09 * column.byte(1) xor
            0x0e * column.byte(2) xor
            0x0b * column.byte(3)
        ).value);
        result_word_bytes[3].setValue((
            0x0b * column.byte(0) xor
            0x0d * column.byte(1) xor
            0x09 * column.byte(2) xor
            0x0e * column.byte(3)
        ).value);
    }

    return Word(result_word_bytes);
}

//OPERATORS
ostream &operator <<(ostream &os, Word word) {
    for (int i = 0; i < 4; ++i) {
        if(word.getByte(i) < 16) os << '0';
        os<<hex<<word.getByte(i) << dec;
    }
    return os;
}
Word operator xor(Word word_a, Word word_b) {
    Word new_word = Word();
    for (int i = 0; i < 4; ++i) new_word.bytes[i] = new Byte(0);
    for (int i = 0; i < 4; ++i) new_word.bytes[i]->setValue((*word_a.bytes[i] xor *word_b.bytes[i]).value);
    return new_word;
}


