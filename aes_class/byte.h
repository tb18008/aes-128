//
// Created by TB18008 on 14.11.2021..
//

#ifndef AES_128_BYTE_H
#define AES_128_BYTE_H

#include <iostream>
#include <cmath>
#include "sbox.h"

using namespace std;

class Byte {
public:
    //PROPERTIES
    bool bits[8];
    int value;

    //CONSTRUCTORS
    explicit Byte(int value = 0);
    explicit Byte(const string& hex);

    //METHODS
    void setValue(int value);
    Byte substituteByte(bool inverse = false);
    Byte shiftDiscard(bool left = true);

    //OPERATORS

    //Compare if two bytes have the same value and bits
    friend bool operator==(const Byte& byte_a,const Byte& byte_b);
    friend bool operator!=(const Byte& byte_a,const Byte& byte_b);

    friend ostream& operator<<(ostream& os, Byte byte);

    //XOR using C++ bitwise operator ^
    friend Byte operator xor(Byte byte_a, Byte byte_b);
    friend Byte operator xor(Byte* byte_a, Byte byte_b);
    friend Byte operator xor(Byte byte_a, Byte* byte_b);

    //Multiplication in Rijndael's finite field
    friend Byte operator *(Byte byte_a, Byte byte_b);
    friend Byte operator *(int byte_value, Byte byte_b);
    friend Byte operator *(Byte byte_a, int byte_value);

};


#endif //AES_128_BYTE_H
