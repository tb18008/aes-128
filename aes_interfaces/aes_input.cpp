//
// Created by TB18008 on 28.11.2021..
//
#include <iostream>
#include <string>
#include "../aes_class/aes.h"

using namespace std;

static void userInputEncrypt(){
    string plaintext, key, ciphertext;

    bool invalid_input = true;

    while (invalid_input){
        cout << "Enter plaintext (hex):\n";
        getline (cin,plaintext);
        if(plaintext.length() != 32) cout << "(text must be 32 characters)" << endl;
        else invalid_input = false;
    }
    invalid_input = true;
    while (invalid_input){
        cout << "Enter key (hex):\n";
        getline (cin,key);
        if(key.length() != 32) cout << "(text must be 32 characters)" << endl;
        else invalid_input = false;
    }
    plaintext.resize(32);
    key.resize(32);
    ciphertext.resize(32);
    AES aes;
    aes.encryptBlock(plaintext, key, ciphertext);
    cout << "Ciphertext (hex):\n" << ciphertext << endl;
}
static void userInputDecrypt(){
    string ciphertext, key, plaintext;

    bool invalid_input = true;

    while (invalid_input){
        cout << "Enter ciphertext (hex):\n";
        getline (cin,ciphertext);
        if(ciphertext.length() != 32) cout << "(text must be 32 characters)" << endl;
        else invalid_input = false;
    }
    invalid_input = true;
    while (invalid_input){
        cout << "Enter key (hex):\n";
        getline (cin,key);
        if(key.length() != 32) cout << "(text must be 32 characters)" << endl;
        else invalid_input = false;
    }
    ciphertext.resize(32);
    key.resize(32);
    plaintext.resize(32);
    AES aes;
    aes.decryptBlock(ciphertext, key, plaintext);
    cout << "Plaintext (hex):\n" << plaintext << endl;
}