//
// Created by TB18008 on 28.11.2021..
//

#include "../aes_class/aes.h"
#include <fstream>

//Count bytes, blocks and last block size
static bool countBlocks(const string &file_name, int &whole_blocks_count, int &last_block_size){
    ifstream infile(file_name, ios::binary);
    if(!infile.is_open())   return false;   //Couldn't read file

    infile.seekg(0, ios::end);
    int file_size = infile.tellg();

    whole_blocks_count = file_size / 16;
    last_block_size = file_size % 16;

    infile.close();

    return true;
}
static bool isBlockPadded(Block& last_block, int& last_block_size){
    last_block_size = last_block.getByte(15);
    if(last_block_size > 0 && last_block_size < 16){
        //Check if the values before last byte are zero
        for (int i = last_block_size; i < 15; ++i) {
            if(last_block.getByte(i) != 0) return false;
        }
        return true;
    }
    else return false;
}

static Block readWholeBlock(ifstream &infile){
    //Read file
    unsigned char byte_value;
    Byte block_bytes[16];
    for (int i = 0; i < 16; ++i) {
        byte_value = infile.get();  //Get 8 bit value from file
        block_bytes[i] = Byte((int)byte_value); //Create a new byte using the value
    }

    Block new_block = Block(block_bytes);   //Create a block using gathered bytes

    return new_block;
}
static Block readLastBlock(ifstream &infile, int last_block_size){
    //Read file
    unsigned char byte_value;
    Byte block_bytes[16];
    for (int i = 0; i < last_block_size; ++i) {
        byte_value = infile.get();  //Get 8 bit value from file
        block_bytes[i] = Byte((int)byte_value); //Create a new byte using the value
    }

    //Apply padding
    for (int i = last_block_size; i < 15; ++i) block_bytes[i] = Byte(0);  //Fill rest with zeros
    block_bytes[15] = Byte(last_block_size);    //Save how long the block was

    Block new_block = Block(block_bytes);   //Create a block using gathered bytes

    return new_block;
}
static bool readBlocksFromFile(const string& input_file_name, Block*& whole_blocks, int& whole_blocks_count, Block& last_block, int& last_block_size){
    bool file_valid;
    file_valid = countBlocks(input_file_name,whole_blocks_count,last_block_size);

    if(!file_valid) return false;   //Couldn't read file
    if(whole_blocks_count == 0 && last_block_size == 0) return true;   //Nothing to encrypt

    //If last block is incomplete then whole block count should be increased
    if(last_block_size > 0) whole_blocks_count++;
    whole_blocks = new Block[whole_blocks_count];

    ifstream infile = ifstream(input_file_name, ios::binary);
    //Read all blocks from file
    for (int i = 0; i < whole_blocks_count-1; ++i) whole_blocks[i].setBlock(readWholeBlock(infile));    //Read complete blocks
    if(last_block_size > 0) whole_blocks[whole_blocks_count-1].setBlock(readLastBlock(infile, last_block_size));    //Read last block
    else whole_blocks[whole_blocks_count - 1].setBlock(readWholeBlock(infile));    //Read complete blocks

    last_block_size = 0;    //Continue everything like last block isn't there

    infile.close();

    return true;
}

static void writeBlockToFile(ofstream &outfile, Block &block_to_write, bool padded_block = false){

    char character;
    int byte_count = 16;
    if(padded_block) byte_count = block_to_write.getByte(15);

    for (int i = 0; i < byte_count; ++i) {
        character = (char)block_to_write.getByte(i);
        outfile.write(&character,1);
    }
}
static void writeBlocksToFile(const string& output_file_name, Block*& whole_blocks, int whole_blocks_count, Block& last_block, int last_block_size){
    ofstream outfile = ofstream(output_file_name, ios::binary);
    //Write all blocks to file
    for (int i = 0; i < whole_blocks_count; ++i) writeBlockToFile(outfile,whole_blocks[i],false);
    if(last_block_size > 0) writeBlockToFile(outfile,last_block,true);    //Write last block

    outfile.close();
}

static bool encryptFile(const string& input_file_name, Block init_vector, KeyBlock key, const string& output_file_name){

    int whole_blocks_count, last_block_size;
    Block* whole_blocks;
    Block last_block;

    bool file_valid;
    file_valid = readBlocksFromFile(input_file_name, whole_blocks, whole_blocks_count, last_block, last_block_size);
    if(!file_valid) return false;
    if(whole_blocks_count == 0 && last_block_size == 0){    //Nothing to encrypt, create empty file
        ofstream outfile = ofstream(output_file_name, ios::binary);
        outfile.close();
        return true;
    }

    //Transform blocks using encryption
    AES aes;
    //First block
    whole_blocks[0].setBlock(whole_blocks[0] xor init_vector);  //Apply initialization vector
    whole_blocks[0].setBlock(aes.encryptBlock(whole_blocks[0],key));  //Encrypt with key
    //Blocks in the middle
    for (int i = 1; i < whole_blocks_count; ++i) {
        whole_blocks[i].setBlock(whole_blocks[i] xor whole_blocks[i-1]);    //Apply previous ciphertext
        whole_blocks[i].setBlock(aes.encryptBlock(whole_blocks[i],key));  //Encrypt with key
    }

    writeBlocksToFile(output_file_name, whole_blocks, whole_blocks_count, last_block, 0);

    return true;
}
static bool decryptFile(const string& input_file_name, Block init_vector, KeyBlock key, const string& output_file_name){

    int whole_blocks_count, last_block_size;
    Block* whole_blocks;
    Block last_block;

    bool file_valid;
    file_valid = readBlocksFromFile(input_file_name,whole_blocks,whole_blocks_count,last_block,last_block_size);
    if(!file_valid) return false;
    if(whole_blocks_count == 0 && last_block_size == 0){    //Nothing to encrypt, create empty file
        ofstream outfile = ofstream(output_file_name, ios::binary);
        outfile.close();
        return true;
    }

    //Transform blocks using decryption
    AES aes;
    Block previous_ciphertext;
    Block current_ciphertext;

    previous_ciphertext.setBlock(init_vector);  //Save previous ciphertext before it is overwritten
    //Blocks in the middle
    for (int i = 0; i < whole_blocks_count; ++i) {
        current_ciphertext.setBlock(whole_blocks[i]);
        whole_blocks[i].setBlock(aes.decryptBlock(whole_blocks[i],key));  //Decrypt with key
        whole_blocks[i].setBlock(whole_blocks[i] xor previous_ciphertext);    //Apply previous ciphertext

        previous_ciphertext.setBlock(current_ciphertext);  //Save previous ciphertext before it is overwritten
    }
    //Check if last block was padded and remove the padding
    last_block.setBlock(whole_blocks[whole_blocks_count - 1]);
    bool last_block_padded = isBlockPadded(last_block, last_block_size);
    if(last_block_padded) whole_blocks_count--;
    writeBlocksToFile(output_file_name, whole_blocks, whole_blocks_count, last_block, last_block_padded * last_block_size);

    return true;
}