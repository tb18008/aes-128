//
// Created by TB18008 on 21.11.2021..
//

#include <iostream>
#include "../aes_class/block.h"
#include "../aes_class/key_block.h"
#include "../aes_class/aes.h"
#include "../aes_interfaces/aes_file.cpp"

static void testKeyExpansion(){
    KeyBlock keys[11];
    keys[0] = KeyBlock("000102030405060708090a0b0c0d0e0f");

    for (int i = 1; i < 11; ++i) keys[i].setBlock(keys[i-1].getRoundKey(i));    //Generate keys

    KeyBlock expected_keys[11] = {
            KeyBlock("000102030405060708090a0b0c0d0e0f"),
            KeyBlock("d6aa74fdd2af72fadaa678f1d6ab76fe"),
            KeyBlock("b692cf0b643dbdf1be9bc5006830b3fe"),
            KeyBlock("b6ff744ed2c2c9bf6c590cbf0469bf41"),
            KeyBlock("47f7f7bc95353e03f96c32bcfd058dfd"),
            KeyBlock("3caaa3e8a99f9deb50f3af57adf622aa"),
            KeyBlock("5e390f7df7a69296a7553dc10aa31f6b"),
            KeyBlock("14f9701ae35fe28c440adf4d4ea9c026"),
            KeyBlock("47438735a41c65b9e016baf4aebf7ad2"),
            KeyBlock("549932d1f08557681093ed9cbe2c974e"),
            KeyBlock("13111d7fe3944a17f307a78b4d2b30c5"),
    };

    //Compare
    for (int i = 0; i < 11; ++i) {
        if(expected_keys[i]!=keys[i]){
            cout << "Key Expansion [ERROR]"<<endl;
            cout << "Expected key:\t" << expected_keys[i]<<endl;
            cout << "Actual key:\t" << keys[i]<<endl;
            return;
        }
    }
    cout << "Key Expansion [OK]"<<endl;
}
static void testBlockXor(){
    Block block_a[11] = {
            Block("00112233445566778899aabbccddeeff"),
            Block("5f72641557f5bc92f7be3b291db9f91a"),
            Block("ff87968431d86a51645151fa773ad009"),
            Block("4c9c1e66f771f0762c3f868e534df256"),
            Block("6385b79ffc538df997be478e7547d691"),
            Block("f4bcd45432e554d075f1d6c51dd03b3c"),
            Block("9816ee7400f87f556b2c049c8e5ad036"),
            Block("c57e1c159a9bd286f05f4be098c63439"),
            Block("baa03de7a1f9b56ed5512cba5f414d23"),
            Block("e9f74eec023020f61bf2ccf2353c21c7"),
            Block("7ad5fda789ef4e272bca100b3d9ff59f"),
    };
    Block block_b[11] = {
            Block("000102030405060708090a0b0c0d0e0f"),
            Block("d6aa74fdd2af72fadaa678f1d6ab76fe"),
            Block("b692cf0b643dbdf1be9bc5006830b3fe"),
            Block("b6ff744ed2c2c9bf6c590cbf0469bf41"),
            Block("47f7f7bc95353e03f96c32bcfd058dfd"),
            Block("3caaa3e8a99f9deb50f3af57adf622aa"),
            Block("5e390f7df7a69296a7553dc10aa31f6b"),
            Block("14f9701ae35fe28c440adf4d4ea9c026"),
            Block("47438735a41c65b9e016baf4aebf7ad2"),
            Block("549932d1f08557681093ed9cbe2c974e"),
            Block("13111d7fe3944a17f307a78b4d2b30c5"),
    };
    Block expected_block[11] = {
            Block("00102030405060708090a0b0c0d0e0f0"),
            Block("89d810e8855ace682d1843d8cb128fe4"),
            Block("4915598f55e5d7a0daca94fa1f0a63f7"),
            Block("fa636a2825b339c940668a3157244d17"),
            Block("247240236966b3fa6ed2753288425b6c"),
            Block("c81677bc9b7ac93b25027992b0261996"),
            Block("c62fe109f75eedc3cc79395d84f9cf5d"),
            Block("d1876c0f79c4300ab45594add66ff41f"),
            Block("fde3bad205e5d0d73547964ef1fe37f1"),
            Block("bd6e7c3df2b5779e0b61216e8b10b689"),
            Block("69c4e0d86a7b0430d8cdb78070b4c55a"),
    };
    Block result;
    for (int i = 0; i < 11; ++i) {
        result = (block_a[i] xor block_b[i]);
        if(result != expected_block[i]){
            cout << "Block XOR [ERROR]"<<endl;
            cout << "Expected block:\t" << expected_block[i]<<endl;
            cout << "Actual block:\t" << result<<endl;
            return;
        }
    }
    cout << "Block XOR [OK]"<<endl;
}
static void testBlockSubstitute(){
    Block block_a[10] = {
            Block("00102030405060708090a0b0c0d0e0f0"),
            Block("89d810e8855ace682d1843d8cb128fe4"),
            Block("4915598f55e5d7a0daca94fa1f0a63f7"),
            Block("fa636a2825b339c940668a3157244d17"),
            Block("247240236966b3fa6ed2753288425b6c"),
            Block("c81677bc9b7ac93b25027992b0261996"),
            Block("c62fe109f75eedc3cc79395d84f9cf5d"),
            Block("d1876c0f79c4300ab45594add66ff41f"),
            Block("fde3bad205e5d0d73547964ef1fe37f1"),
            Block("bd6e7c3df2b5779e0b61216e8b10b689"),
    };
    Block expected_block[10] = {
            Block("63cab7040953d051cd60e0e7ba70e18c"),
            Block("a761ca9b97be8b45d8ad1a611fc97369"),
            Block("3b59cb73fcd90ee05774222dc067fb68"),
            Block("2dfb02343f6d12dd09337ec75b36e3f0"),
            Block("36400926f9336d2d9fb59d23c42c3950"),
            Block("e847f56514dadde23f77b64fe7f7d490"),
            Block("b415f8016858552e4bb6124c5f998a4c"),
            Block("3e175076b61c04678dfc2295f6a8bfc0"),
            Block("5411f4b56bd9700e96a0902fa1bb9aa1"),
            Block("7a9f102789d5f50b2beffd9f3dca4ea7"),
    };
    Block result;
    for (int i = 0; i < 10; ++i) {
        result.setBlock(block_a[i].substituteBytes());
        if(result != expected_block[i]){
            cout << "Block Substitute [ERROR]"<<endl;
            cout << "Expected block:\t" << expected_block[i]<<endl;
            cout << "Actual block:\t" << result<<endl;
            return;
        }
    }
    cout << "Block Substitute [OK]"<<endl;
}
static void testBlockShiftRows(){
    Block block_a[10] = {
            Block("63cab7040953d051cd60e0e7ba70e18c"),
            Block("a761ca9b97be8b45d8ad1a611fc97369"),
            Block("3b59cb73fcd90ee05774222dc067fb68"),
            Block("2dfb02343f6d12dd09337ec75b36e3f0"),
            Block("36400926f9336d2d9fb59d23c42c3950"),
            Block("e847f56514dadde23f77b64fe7f7d490"),
            Block("b415f8016858552e4bb6124c5f998a4c"),
            Block("3e175076b61c04678dfc2295f6a8bfc0"),
            Block("5411f4b56bd9700e96a0902fa1bb9aa1"),
            Block("7a9f102789d5f50b2beffd9f3dca4ea7"),
    };
    Block expected_block[10] = {
            Block("6353e08c0960e104cd70b751bacad0e7"),
            Block("a7be1a6997ad739bd8c9ca451f618b61"),
            Block("3bd92268fc74fb735767cbe0c0590e2d"),
            Block("2d6d7ef03f33e334093602dd5bfb12c7"),
            Block("36339d50f9b539269f2c092dc4406d23"),
            Block("e8dab6901477d4653ff7f5e2e747dd4f"),
            Block("b458124c68b68a014b99f82e5f15554c"),
            Block("3e1c22c0b6fcbf768da85067f6170495"),
            Block("54d990a16ba09ab596bbf40ea111702f"),
            Block("7ad5fda789ef4e272bca100b3d9ff59f"),
    };
    Block result;
    for (int i = 0; i < 10; ++i) {
        result.setBlock(block_a[i].shiftRows(false));
        if(result != expected_block[i]){
            cout << "Block Shift Rows [ERROR]"<<endl;
            cout << "Expected block:\t" << expected_block[i]<<endl;
            cout << "Actual block:\t" << result<<endl;
            return;
        }
    }
    cout << "Block Shift Rows [OK]"<<endl;
}
static void testBlockMixColumns(){
    Block block_a[10] = {
            Block("6353e08c0960e104cd70b751bacad0e7"),
            Block("a7be1a6997ad739bd8c9ca451f618b61"),
            Block("3bd92268fc74fb735767cbe0c0590e2d"),
            Block("2d6d7ef03f33e334093602dd5bfb12c7"),
            Block("36339d50f9b539269f2c092dc4406d23"),
            Block("e8dab6901477d4653ff7f5e2e747dd4f"),
            Block("b458124c68b68a014b99f82e5f15554c"),
            Block("3e1c22c0b6fcbf768da85067f6170495"),
            Block("54d990a16ba09ab596bbf40ea111702f"),
    };
    Block expected_block[10] = {
            Block("5f72641557f5bc92f7be3b291db9f91a"),
            Block("ff87968431d86a51645151fa773ad009"),
            Block("4c9c1e66f771f0762c3f868e534df256"),
            Block("6385b79ffc538df997be478e7547d691"),
            Block("f4bcd45432e554d075f1d6c51dd03b3c"),
            Block("9816ee7400f87f556b2c049c8e5ad036"),
            Block("c57e1c159a9bd286f05f4be098c63439"),
            Block("baa03de7a1f9b56ed5512cba5f414d23"),
            Block("e9f74eec023020f61bf2ccf2353c21c7"),
    };
    Block result;
    for (int i = 0; i < 10; ++i) {
        result.setBlock(block_a[i].mixColumns(false));
        if(result != expected_block[i]){
            cout << "Block Mix Columns [ERROR]"<<endl;
            cout << "Expected block:\t" << expected_block[i]<<endl;
            cout << "Actual block:\t" << result<<endl;
            return;
        }
    }
    cout << "Block Mix Columns [OK]"<<endl;
}
static void testByteShiftDiscard(){

    Byte byte_before;
    Byte byte_after;
    Byte expected_byte;

    //Shift left test
    for (int i = 0; i < 256; ++i) {
        byte_before = Byte(i);
        byte_after = byte_before;
        byte_after.setValue(byte_after.shiftDiscard(true).value);

        expected_byte = Byte((i*2) % 256);

        if(byte_after != expected_byte){
            cout << "Byte Shift Discard Test (left) [ERROR]"<<endl;
            cout << "Byte before shift:\t" << byte_before << endl;
            cout << "Byte after shift:\t" << byte_after << endl;
            cout << "Expected byte:\t"<<expected_byte<<endl;
            return;
        }
    }
    //Shift right test
    for (int i = 0; i < 256; ++i) {
        byte_before = Byte(i);
        byte_after = byte_before;
        byte_after.setValue(byte_after.shiftDiscard(false).value);

        expected_byte = Byte(i/2);

        if(byte_after != expected_byte){
            cout << "Byte Shift Discard (right) [ERROR]" << endl;
            cout << "Byte before shift:\t" << byte_before << endl;
            cout << "Byte after shift:\t" << byte_after << endl;
            cout << "Expected byte:\t"<<expected_byte<<endl;
            return;
        }
    }
    cout << "Byte Shift Discard [OK]" << endl;
}
static void testByteMultiplication(){
    //Test samples from FIPS-197-AES all examples of multiplication
    unsigned char byte_a_values[12] {
            0x53, 0xca, 0x57, 0x57, 0x03, 0x01, 0x01, 0x02, 0x57, 0x57, 0x57, 0x57
    };
    unsigned char byte_b_values[12] {
            0xca, 0x53, 0x83, 0x13, 0x0e, 0x09, 0x0d, 0x0b, 0x02, 0x04, 0x08, 0x10
    };
    unsigned char expected_values[12] {
            0x01, 0x01, 0xc1, 0xfe, 0x12, 0x09, 0x0d, 0x16, 0xae, 0x47, 0x8e, 0x07
    };

    Byte byte_a, byte_b, result_byte, expected_byte;
    for (int i = 0; i < 12; ++i) {
        byte_a = Byte(byte_a_values[i]);
        byte_b = Byte(byte_b_values[i]);
        result_byte = byte_a * byte_b;
        expected_byte = Byte(expected_values[i]);
        if(result_byte != expected_byte){
            cout << "Byte Multiplication [ERROR]" << endl;
            cout << "Byte A:\t" << byte_a << endl;
            cout << "Byte B:\t" << byte_b << endl;
            cout << "A * B:\t" << result_byte << endl;
            cout << "Expected byte:\t"<<expected_byte<<endl;
            return;
        }
    }
    cout << "Byte Multiplication [OK]" << endl;
}

static void testAesEncryption() {
    AES aes;
    string ciphertext;
    ciphertext.resize(33);
    aes.encryptBlock("00112233445566778899aabbccddeeff","000102030405060708090a0b0c0d0e0f",ciphertext);
    Block result_output = Block(ciphertext);
    Block expected_output = Block("69c4e0d86a7b0430d8cdb78070b4c55a");
    if(result_output!=expected_output){
        cout << "AES Encryption [ERROR]" << endl;
        cout << "Expected output:\t" << expected_output << endl;
        cout << "Actual output:\t" << result_output << endl;
        return;
    }
    cout << "AES Encryption [OK]" << endl;
}

static void testAesDecryption() {
    AES aes;
    string plaintext;
    plaintext.resize(33);
    aes.decryptBlock("69c4e0d86a7b0430d8cdb78070b4c55a","000102030405060708090a0b0c0d0e0f",plaintext);
    Block result_output = Block(plaintext);
    Block expected_output = Block("00112233445566778899aabbccddeeff");
    if(result_output!=expected_output){
        cout << "AES Decryption [ERROR]" << endl;
        cout << "Expected output:\t" << expected_output << endl;
        cout << "Actual output:\t" << result_output << endl;
        return;
    }
    cout << "AES Decryption [OK]" << endl;
}

static bool filesEqual(const string &file_name_a,const string &file_name_b){
    //Open and check if files successfully opened
    ifstream infile_a = ifstream(file_name_a, ios::binary);
    ifstream infile_b = ifstream(file_name_b, ios::binary);
    if(!infile_a.is_open() || !infile_b.is_open()) return false;

    unsigned char byte_value_a, byte_value_b;
    //Compare bytes from start to end of file a
    while (!infile_a.eof()){
        if(infile_a.get() != infile_b.get()) return false;
    }
    if(!infile_b.eof()) return false;   //File b is longer than file a

    //Close files
    infile_a.close();
    infile_b.close();

    return true;    //Files are equal
}

static void testAesFileEncryption(){

    string files[7] = {
            "non_existent",
            "empty",
            "complete_block",
            "incomplete_block",
            "complete_blocks",
            "incomplete_blocks",
            "combination"
    };
    string plaintext_file_name, encrypted_file_name, decrypted_file_name, expected_file_name;

    plaintext_file_name = "../tests/"+files[0]+"_plaintext.txt";
    encrypted_file_name = "../tests/"+files[0]+"_encrypted.txt";
    decrypted_file_name = "../tests/"+files[0]+"_decrypted.txt";

    if(encryptFile(plaintext_file_name,
                   Block(""),
                   KeyBlock(""),
                   encrypted_file_name)){
        cout << "AES File Encryption [ERROR]" << endl;
        cout << plaintext_file_name << " does not exist but function returned true" << endl;
        return;
    }
    if(decryptFile(encrypted_file_name,
                   Block(""),
                   KeyBlock(""),
                   decrypted_file_name)){
        cout << "AES File Encryption [ERROR]" << endl;
        cout << encrypted_file_name << " does not exist but function returned true" << endl;
        return;
    }


    for (int i = 1; i < 7; ++i) {
        plaintext_file_name = "../tests/"+files[i]+"_plaintext.txt";
        encrypted_file_name = "../tests/"+files[i]+"_encrypted.txt";
        decrypted_file_name = "../tests/"+files[i]+"_decrypted.txt";
        expected_file_name = "../tests/"+files[i]+"_expected.txt";

        encryptFile(plaintext_file_name,
                    Block(""),
                    KeyBlock(""),
                    encrypted_file_name);
        decryptFile(encrypted_file_name,
                    Block(""),
                    KeyBlock(""),
                    decrypted_file_name);

        if(!filesEqual(decrypted_file_name,expected_file_name)){
            cout << "AES File Encryption [ERROR]" << endl;
            cout << decrypted_file_name << " is not equal to " << expected_file_name << endl;
            return;
        }

        remove(encrypted_file_name.c_str());
        remove(decrypted_file_name.c_str());
    }
    cout << "AES File Encryption [OK]" << endl;

}

static void runAllTests(){
    testByteShiftDiscard();
    testByteMultiplication();

    testKeyExpansion();

    testBlockXor();
    testBlockSubstitute();
    testBlockShiftRows();
    testBlockMixColumns();

    testAesEncryption();
    testAesDecryption();

    testAesFileEncryption();
}